/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  // {
  //   url: "/apps/email",
  //   name: "Email",
  //   slug: "email",
  //   icon: "MailIcon",
  //   i18n: "Email",
  // },|

    {
        url: '/dashboard',
        name: 'Dashboard',
        tagColor: 'warning',
        icon: 'HomeIcon',
        i18n: 'Dashboard',
        // submenu: [
            // {
            //     url: '/dashboard/analytics',
            //     name: 'Analytics',
            //     slug: 'dashboard-analytics',
            //     i18n: 'Analytics'
            // },
            // {
            //     url: '/dashboard/ecommerce',
            //     name: 'eCommerce',
            //     slug: 'dashboard-ecommerce',
            //     i18n: 'eCommerce'
            // }
        // ]
    },
    {
        url: null,
        name: 'Image',
        icon: 'UserIcon',
        i18n: 'Clients management',
        submenu: [
            {
                url: '/add-client',
                name: 'Add New Client',
                slug: '',
                i18n: 'Add New Client'
            },
            {
                url: '/manage-clients',
                name: 'Manage Clients',
                slug: '',
                i18n: 'Manage Clients'
            }
        ]
    },

    {
        url: null,
        name: 'Sites Management',
        icon: 'SettingsIcon',
        i18n: 'Sites Management',
        submenu: [
            {
                url: '/products/add',
                name: 'Add New Products',
                slug: '',
                i18n: 'Add New Site'
            },
            {
                url: '/products/view',
                name: 'View Products',
                slug: '',
                i18n: 'Manage Sites'
            }
        ]
    },
    // {
    //     url: null,
    //     name: 'Country Taxes',
    //     icon: 'HomeIcon',
    //     i18n: 'Country Taxes',
    //     submenu: [
    //         {
    //             url: '/country_taxes',
    //             name: 'Taxes',
    //             slug: '',
    //             i18n: 'Taxes'
    //         }
    //     ]
    // },
    // {
    //     url: null,
    //     name: 'Payment Method',
    //     icon: 'HomeIcon',
    //     i18n: 'Payment Method',
    //     submenu: [
    //         {
    //             url: '/payment_method',
    //             name: 'Add New',
    //             slug: '',
    //             i18n: 'Add New'
    //         }
    //     ]
    // },
    // {
    //     url: null,
    //     name: 'Generate Voucher Number',
    //     icon: 'PackageIcon',
    //     i18n: 'Generate Voucher Number',
    //     submenu: [
    //         {
    //             url: '/generate_voucher_management',
    //             name: 'Add New',
    //             slug: '',
    //             i18n: 'Add New'
    //         }
    //     ]
    // },
    // {
    //     url: null,
    //     name: 'Levels',
    //     icon: 'PackageIcon',
    //     i18n: 'Levels',
    //     submenu: [
    //         {
    //             url: '/levels',
    //             name: 'Add New',
    //             slug: '',
    //             i18n: 'Add New'
    //         }
    //     ]
    // }
]



